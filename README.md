# ES6 compiler

This compiler compile you es6 code to es5 wich save project structure.  
To install in system do ```npm i && npm link``` and run **es6compile** in you project directory.  
Also you can run ```./bin/es6compile [path to compile]```  
path to compile (optional)- its directory path to you code.  

## How its work  

es6compile - search in directory and all subdirectories files like *.es6.js, compile wich babel and write to *.js file wich save path file.