
var glob = require("glob");
var babel = require("@babel/core");
const fs = require('fs');

const path = '**/*.es6.js';
let DIR = process.cwd()+'/';
const args = process.argv.slice(2);

if(args.length) {
    DIR = args[0];
}

console.info('Base search directory = '+DIR+path);

var writeToFile= function(filename, code) {
    fs.writeFile(filename, code, function(err) {
        if(err) {
            console.log("[ERROR] ",err);
            return ;
        }
        console.log("[SUCCES] "+filename.replace(DIR, ''));
    });
}

glob(DIR+path, function(err, files) {
    files.forEach(file => {
        babel.transformFileAsync(file, {
            presets: ["@babel/preset-env"]
          }).then(result => {
            writeToFile(file.replace(/\.es6\.js$/, ".js"),result.code);
          });
    });
});
